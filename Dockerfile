ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG} as base

COPY k6.tar.gz /
RUN tar -zxf /k6.tar.gz --strip-components=1 -C /usr/local/bin/

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

COPY --from=base /usr/local/bin/k6 /usr/local/bin/k6

RUN groupadd -g 12345 k6 && \
    useradd -r -u 12345 -m -s /sbin/nologin -g k6 k6 && \
    chown root:root /usr/local/bin/k6 && \
    dnf upgrade -y && \
    dnf clean all && \
    rm -rf /var/cache/dnf

# COPY --from=base /usr/local/bin/k6 /usr/local/bin/k6


USER 12345
HEALTHCHECK NONE

ENTRYPOINT ["k6"]
